/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Palabra;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import ufps.util.varios.ArchivoLeerURL;
import util.ufps.colecciones_seed.VectorGenerico;

/**
 * @author María Alexandra Velásquez Jaimes
 * @author José Julián Álvarez Reyes
 */
public class Diccionario {

    private VectorGenerico<Palabra> palabras;

    public Diccionario() {
    }

    /**
     * Carga un objeto diccionario apartir de las palabras de una url
     *
     * @param urlDiccionario url de las palabras a cargar en el diccionario
     * @throws java.lang.Exception si la url proporcionada no existe.
     */
    public void cargar(String urlDiccionario) throws Exception{
        ArchivoLeerURL archivo = new ArchivoLeerURL(urlDiccionario);
        Object datos[] = archivo.leerArchivo();
        if(datos.length <= 0)
            throw new Exception("La url proporcionada no existe :(");
        palabras = new VectorGenerico(datos.length);
        for (Object dato : datos) {
            Palabra p = new Palabra(dato.toString());
            palabras.add(p);
        }
    }

    /**
     * Busca una palabra dada en el diccionario
     *
     * @param palabra_A_buscar Palabra la cual se buscara en el diccionario
     * @return Un valor true, si la palabra esta en el diccionario y false en el
     * caso que la palabra no este
     */
    public boolean buscarPalabra(String palabra_A_buscar) {

        if (palabra_A_buscar.isEmpty() || palabra_A_buscar.isBlank()) {
            throw new RuntimeException("No se puede buscar patrones vacíos");
        }
        boolean salida = false;
        //Debe convertir la palabra_A_buscar a un objeto palabra:
        Palabra myPalabra = new Palabra(palabra_A_buscar);
        for (int i = 0; i < palabras.length() && salida != true; i++) {
            if (myPalabra.equals(palabras.get(i))) {
                salida = true;
            }
        }
        return salida;
    }

    /**
     * Busca un patrón determinado en el documento, regresando -1 si dicho
     * patron no se encuentra, y si se encuantra regresa cuantas veces está,
     * ademas de cambiar el antiguo patron por uno nuevo a elección
     *
     * @param patron Un patron a buscar en el diccionario
     * @param patron_Nuevo un nuevo patron por el cual cambiar el anterior
     * @return La cantidad de veces que se repite el patron dado, y si no se
     * encuentra regresa -1, ademas de cambiarlo por el patrón nuevo.
     */
    public int buscarPatron(String patron, String patron_Nuevo) {
        int salida = 0;
        Palabra myPatron = new Palabra(patron);
        Palabra myPatron_Nuevo = new Palabra(patron_Nuevo);
        int tamañoMyPatron = myPatron.getCaracteres().length();
        for (int i = 0; i < this.palabras.length(); i++) {
            int cnt = 0;
            if (tamañoMyPatron <= this.palabras.get(i).getCaracteres().length()) {
                for (int j = 0; j <= (this.palabras.get(i).getCaracteres().length() - tamañoMyPatron); j++) {
                    if (this.palabras.get(i).getCaracteres().get(j) == myPatron.getCaracteres().get(0)) {
                        cnt++;
                        for (int k = 1; k < tamañoMyPatron; k++) {
                            if (myPatron.getCaracteres().get(k) == this.palabras.get(i).getCaracteres().get(j + k)) {
                                cnt++;
                            }
                        }
                        if (cnt == tamañoMyPatron) {
                            salida++;
                            this.palabras.get(i).actualizar(j, myPatron, myPatron_Nuevo);//Donde ocurre la magia
                        }
                        cnt = 0;
                    }
                }
            }
        }
        if (salida == 0) {
            salida--;
        }
        return salida;
    }

    /**
     * Retorna la información del diccionario
     *
     * @return una cadena con la información del diccionario
     */
    public String getImprimir() {
        String msg = "";
        for (int i = 0; i < this.palabras.length(); i++) {
            msg += this.palabras.get(i).getCaracteres().toString() + "\n";
        }
        return msg;
    }

    /**
     * Retorna un vector con las palabras del diccionario
     *
     * @return Retorna un vector con las palabras del diccionario.
     */
    public VectorGenerico<Palabra> getPalabras() {
        return palabras;
    }
    /**
     * Actualiza un vector con las palabras del diccionario
     *
     * @param palabras vector con las palabras del diccionario.
     */
    public void setPalabras(VectorGenerico<Palabra> palabras) {
        this.palabras = palabras;
    }

    /**
     * Crea un archivo formato PDF de la informacion de un numero determinado de
     * lineas del diccionario
     *
     * @param lineas La cantidad de lineas del diccionario a pasar a PDF
     * @param rutaArchivo ruta escogida por el usuario
     * @throws java.lang.Exception si el documento no existe
     *
     */
    public void crearPdf(int lineas, String rutaArchivo) throws Exception {
        
        try {
            Document doc = new Document();
            FileOutputStream archivo = new FileOutputStream(rutaArchivo + ".pdf");
            PdfWriter.getInstance(doc, archivo);
            doc.open();
            doc.add(new Paragraph("PDF creado con " + lineas + " Palabras"));
            doc.add(new Paragraph(""));
            for (int i = 0; i < lineas; i++) {
                doc.add(new Paragraph(this.palabras.get(i).getCaracteres().toString() + "\n"));
            }
            doc.close();

        } catch (Exception e) {
            throw new Exception("El documento no existe");
        }
    }
}
