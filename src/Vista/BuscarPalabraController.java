package Vista;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class BuscarPalabraController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lblTitulo;

    @FXML
    private Label lblTexto;

    @FXML
    private Label lblRespuesta;

    @FXML
    private TextField txtPalabraABuscar;

    @FXML
    private Button cmdCargarPalabras;

    @FXML
    private Button cmdBuscarPalabras;

    @FXML
    private Button cmdImprimirDiccionario;

    @FXML
    private Button cmdBuscarPatron;

    @FXML
    private Button cmdImprimirPDF;

    @FXML
    private ImageView imgLogo;

    @FXML
    private Button cmdBuscar;
    
    @FXML
    private ImageView imgCambiar;

    @FXML
    void buscar(ActionEvent event) {
        String aviso = "";
        boolean encontrado = false;
        Alert alert = null;
        try{
            if(VistaPrincipalController.getDiccionario().getPalabras() == null) 
                throw new Exception("Por favor primero cargue un diccionario.");
            if(txtPalabraABuscar.getText().isBlank())
                throw new Exception("Por favor ingrese una palabra a buscar.");
            encontrado = VistaPrincipalController.getDiccionario().buscarPalabra(txtPalabraABuscar.getText());
            if(encontrado) {
                lblRespuesta.setText("Palabra encontrada :)");
            }
            else {
                lblRespuesta.setText("Palabra no encontrada :(");
            }
            
        }
        catch(Exception e) {
            aviso = "Error al buscar palabra: " + e.getMessage();
            alert = new Alert(Alert.AlertType.ERROR, aviso, ButtonType.OK);
            alert.showAndWait();
        }
    }

    @FXML
    void buscarPalabras(ActionEvent event) throws IOException {
        actualizarVentana("buscarPalabra.fxml");
    }

    @FXML
    void buscarPatron(ActionEvent event) throws IOException {
        actualizarVentana("buscarPatron.fxml");
    }

    @FXML
    void cargarPalabras(ActionEvent event) throws IOException {
        actualizarVentana("cargarPalabras.fxml");
    }

    @FXML
    void imprimirDiccionario(ActionEvent event) throws IOException {
        actualizarVentana("imprimirDiccionario.fxml");
    }

    @FXML
    void imprimirPDF(ActionEvent event) throws IOException {
        actualizarVentana("imprimirPDF.fxml");
    }
    
    private void actualizarVentana(String archivo) throws IOException {
        Stage stage = VistaPrincipalFX.app.getStage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource(archivo));
        Pane root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    @FXML
    void initialize() {
        assert lblTitulo != null : "fx:id=\"lblTitulo\" was not injected: check your FXML file 'buscarPalabra.fxml'.";
        assert lblTexto != null : "fx:id=\"lblTexto\" was not injected: check your FXML file 'buscarPalabra.fxml'.";
        assert lblRespuesta != null : "fx:id=\"lblRespuesta\" was not injected: check your FXML file 'buscarPalabra.fxml'.";
        assert txtPalabraABuscar != null : "fx:id=\"txtPalabraABuscar\" was not injected: check your FXML file 'buscarPalabra.fxml'.";
        assert cmdCargarPalabras != null : "fx:id=\"cmdCargarPalabras\" was not injected: check your FXML file 'buscarPalabra.fxml'.";
        assert cmdBuscarPalabras != null : "fx:id=\"cmdBuscarPalabras\" was not injected: check your FXML file 'buscarPalabra.fxml'.";
        assert cmdImprimirDiccionario != null : "fx:id=\"cmdImprimirDiccionario\" was not injected: check your FXML file 'buscarPalabra.fxml'.";
        assert cmdBuscarPatron != null : "fx:id=\"cmdBuscarPatron\" was not injected: check your FXML file 'buscarPalabra.fxml'.";
        assert cmdImprimirPDF != null : "fx:id=\"cmdImprimirPDF\" was not injected: check your FXML file 'buscarPalabra.fxml'.";
        assert imgLogo != null : "fx:id=\"imgLogo\" was not injected: check your FXML file 'buscarPalabra.fxml'.";
        assert cmdBuscar != null : "fx:id=\"cmdBuscar\" was not injected: check your FXML file 'buscarPalabra.fxml'.";

    }
}
