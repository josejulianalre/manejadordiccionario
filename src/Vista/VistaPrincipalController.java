package Vista;

import Negocio.Diccionario;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * @author María Alexandra Velásquez Jaimes
 * @author José Julián Álvarez Reyes
 */

public class VistaPrincipalController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lblTitulo;

    @FXML
    private Button cmdCargarPalabras;

    @FXML
    private Button cmdBuscarPalabras;

    @FXML
    private Button cmdImprimirdiccionario;

    @FXML
    private Button cmdBuscarPatron;

    @FXML
    private Button cmdImprimirPDF;

    @FXML
    private ImageView imgLogo;

    @FXML
    private Label lblTexto;

    @FXML
    private Label lblNombre2;

    @FXML
    private Label lblNombre1;

    @FXML
    private Label lblCodigo2;

    @FXML
    private Label lblCodigo1;
    
    protected static Diccionario diccionario;

    public static Diccionario getDiccionario() {
        return diccionario;
    }

    public static void setDiccionario(Diccionario diccionario) {
        VistaPrincipalController.diccionario = diccionario;
    }

    @FXML
    void buscarPalabras(ActionEvent event) throws IOException{
        actualizarVentana("buscarPalabra.fxml");
    }

    @FXML
    void buscarPatron(ActionEvent event) throws IOException{
        actualizarVentana("buscarPatron.fxml");
    }

    @FXML
    void cargarPalabras(ActionEvent event) throws IOException{
        actualizarVentana("cargarPalabras.fxml");
    }

    @FXML
    void imprimirPDF(ActionEvent event) throws IOException {
        actualizarVentana("imprimirPDF.fxml");
    }

    @FXML
    void imprimirdiccionario(ActionEvent event) throws IOException {
        actualizarVentana("imprimirDiccionario.fxml");
    }
    
    private void actualizarVentana(String archivo) throws IOException {
        Stage stage = VistaPrincipalFX.app.getStage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource(archivo));
        Pane root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void initialize() {
        assert lblTitulo != null : "fx:id=\"lblTitulo\" was not injected: check your FXML file 'vistaPrincipal.fxml'.";
        assert cmdCargarPalabras != null : "fx:id=\"cmdCargarPalabras\" was not injected: check your FXML file 'vistaPrincipal.fxml'.";
        assert cmdBuscarPalabras != null : "fx:id=\"cmdBuscarPalabras\" was not injected: check your FXML file 'vistaPrincipal.fxml'.";
        assert cmdImprimirdiccionario != null : "fx:id=\"cmdImprimirdiccionario\" was not injected: check your FXML file 'vistaPrincipal.fxml'.";
        assert cmdBuscarPatron != null : "fx:id=\"cmdBuscarPatron\" was not injected: check your FXML file 'vistaPrincipal.fxml'.";
        assert cmdImprimirPDF != null : "fx:id=\"cmdImprimirPDF\" was not injected: check your FXML file 'vistaPrincipal.fxml'.";
        assert imgLogo != null : "fx:id=\"imgLogo\" was not injected: check your FXML file 'vistaPrincipal.fxml'.";
        assert lblTexto != null : "fx:id=\"lblTexto\" was not injected: check your FXML file 'vistaPrincipal.fxml'.";
        assert lblNombre2 != null : "fx:id=\"lblNombre2\" was not injected: check your FXML file 'vistaPrincipal.fxml'.";
        assert lblNombre1 != null : "fx:id=\"lblNombre1\" was not injected: check your FXML file 'vistaPrincipal.fxml'.";
        assert lblCodigo2 != null : "fx:id=\"lblCodigo2\" was not injected: check your FXML file 'vistaPrincipal.fxml'.";
        assert lblCodigo1 != null : "fx:id=\"lblCodigo1\" was not injected: check your FXML file 'vistaPrincipal.fxml'.";
        
        diccionario = new Diccionario();
    }
}
