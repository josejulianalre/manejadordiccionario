package Vista;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class BuscarPatronController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lblTexto1;

    @FXML
    private Label lblRespuesta;

    @FXML
    private TextField txtPatronViejo;

    @FXML
    private Label lblTexto2;

    @FXML
    private TextField txtPatronNuevo;

    @FXML
    private Button cmdCargarPalabras;

    @FXML
    private Button cmdBuscarPalabras;

    @FXML
    private Button cmdImprimirDiccionario;

    @FXML
    private Button cmdBuscarPatron;

    @FXML
    private Button cmdImprimirPDF;

    @FXML
    private Label lblTitulo;

    @FXML
    private ImageView imgLogo;

    @FXML
    private Button cmdIntercambiarPatron;

    @FXML
    void buscarPalabras(ActionEvent event) throws IOException {
        actualizarVentana("buscarPalabra.fxml");
    }

    @FXML
    void buscarPatron(ActionEvent event) throws IOException {
        actualizarVentana("buscarPatron.fxml");
    }

    @FXML
    void cargarPalabras(ActionEvent event) throws IOException {
        actualizarVentana("cargarPalabras.fxml");
    }

    @FXML
    void imprimirDiccionario(ActionEvent event) throws IOException {
        actualizarVentana("imprimirDiccionario.fxml");
    }

    @FXML
    void imprimirPDF(ActionEvent event) throws IOException {
        actualizarVentana("imprimirPDF.fxml");
    }

private void actualizarVentana(String archivo) throws IOException {
        Stage stage = VistaPrincipalFX.app.getStage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource(archivo));
        Pane root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    
    @FXML
    void intercambiarPatron(ActionEvent event) {
        int n;
        String aviso = "";
        Alert alert = null;
        try{
            if(VistaPrincipalController.getDiccionario().getPalabras() == null) 
                throw new Exception("Por favor primero cargue un diccionario.");
            n = VistaPrincipalController.getDiccionario().buscarPatron(txtPatronViejo.getText(), txtPatronNuevo.getText());
            if(n == -1)
                lblRespuesta.setText("No se ha encontrado el patrón solicitado.");
            else
                lblRespuesta.setText("El patron " + txtPatronViejo.getText() + " se intercambio por el patrón " + txtPatronNuevo.getText() + " " + n + " veces.");                
        }
        catch(Exception e) {
            aviso = "Error al crear PDF: " + e.getMessage();
            alert = new Alert(Alert.AlertType.ERROR, aviso, ButtonType.OK);
            alert.showAndWait();
        }
    }

    @FXML
    void initialize() {
        assert lblTexto1 != null : "fx:id=\"lblTexto1\" was not injected: check your FXML file 'buscarPatron.fxml'.";
        assert lblRespuesta != null : "fx:id=\"lblRespuesta\" was not injected: check your FXML file 'buscarPatron.fxml'.";
        assert txtPatronViejo != null : "fx:id=\"txtPatronViejo\" was not injected: check your FXML file 'buscarPatron.fxml'.";
        assert lblTexto2 != null : "fx:id=\"lblTexto2\" was not injected: check your FXML file 'buscarPatron.fxml'.";
        assert txtPatronNuevo != null : "fx:id=\"txtPatronNuevo\" was not injected: check your FXML file 'buscarPatron.fxml'.";
        assert cmdCargarPalabras != null : "fx:id=\"cmdCargarPalabras\" was not injected: check your FXML file 'buscarPatron.fxml'.";
        assert cmdBuscarPalabras != null : "fx:id=\"cmdBuscarPalabras\" was not injected: check your FXML file 'buscarPatron.fxml'.";
        assert cmdImprimirDiccionario != null : "fx:id=\"cmdImprimirDiccionario\" was not injected: check your FXML file 'buscarPatron.fxml'.";
        assert cmdBuscarPatron != null : "fx:id=\"cmdBuscarPatron\" was not injected: check your FXML file 'buscarPatron.fxml'.";
        assert cmdImprimirPDF != null : "fx:id=\"cmdImprimirPDF\" was not injected: check your FXML file 'buscarPatron.fxml'.";
        assert lblTitulo != null : "fx:id=\"lblTitulo\" was not injected: check your FXML file 'buscarPatron.fxml'.";
        assert imgLogo != null : "fx:id=\"imgLogo\" was not injected: check your FXML file 'buscarPatron.fxml'.";
        assert cmdIntercambiarPatron != null : "fx:id=\"cmdIntercambiarPatron\" was not injected: check your FXML file 'buscarPatron.fxml'.";

    }
}
