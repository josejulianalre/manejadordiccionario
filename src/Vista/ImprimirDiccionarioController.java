package Vista;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ImprimirDiccionarioController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lblTexto;

    @FXML
    private Label lblTitulo;

    @FXML
    private Button cmdCargarPalabras;

    @FXML
    private Button cmdBuscarPalabras;

    @FXML
    private Button cmdImprimirDiccionario;

    @FXML
    private Button cmdBuscarPatron;

    @FXML
    private Button cmdImprimirPDF;

    @FXML
    private ImageView lblLogo;

    @FXML
    private TextArea txtDiccionario;

    @FXML
    private Button cmdImprimir;

    @FXML
    private Button cmdBorrar;

    @FXML
    void borrar(ActionEvent event) {
        txtDiccionario.setText("");
    }

    @FXML
    void buscarPalabras(ActionEvent event) throws IOException {
        actualizarVentana("buscarPalabra.fxml");
    }

    @FXML
    void buscarPatron(ActionEvent event) throws IOException {
        actualizarVentana("buscarPatron.fxml");
    }

    @FXML
    void cargarPalabras(ActionEvent event) throws IOException {
        actualizarVentana("cargarPalabras.fxml");
    }

    @FXML
    void imprimir(ActionEvent event) {
        String aviso = "";
        Alert alert = null;
        try{
            if(VistaPrincipalController.getDiccionario().getPalabras() == null) 
                throw new Exception("Por favor primero cargue un diccionario.");
            txtDiccionario.setText(VistaPrincipalController.getDiccionario().getImprimir());
        }
        catch(Exception e) {
            aviso = "Error al imprimir diccionario: " + e.getMessage();
            alert = new Alert(Alert.AlertType.ERROR, aviso, ButtonType.OK);
            alert.showAndWait();
        }
    }

    @FXML
    void imprimirDiccionario(ActionEvent event) throws IOException {
        actualizarVentana("imprimirDiccionario.fxml");
    }

    @FXML
    void imprimirPDF(ActionEvent event) throws IOException {
        actualizarVentana("imprimirPDF.fxml");
    }
    
    private void actualizarVentana(String archivo) throws IOException {
        Stage stage = VistaPrincipalFX.app.getStage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource(archivo));
        Pane root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }


    @FXML
    void initialize() {
        assert lblTexto != null : "fx:id=\"lblTexto\" was not injected: check your FXML file 'imprimirDiccionario.fxml'.";
        assert lblTitulo != null : "fx:id=\"lblTitulo\" was not injected: check your FXML file 'imprimirDiccionario.fxml'.";
        assert cmdCargarPalabras != null : "fx:id=\"cmdCargarPalabras\" was not injected: check your FXML file 'imprimirDiccionario.fxml'.";
        assert cmdBuscarPalabras != null : "fx:id=\"cmdBuscarPalabras\" was not injected: check your FXML file 'imprimirDiccionario.fxml'.";
        assert cmdImprimirDiccionario != null : "fx:id=\"cmdImprimirDiccionario\" was not injected: check your FXML file 'imprimirDiccionario.fxml'.";
        assert cmdBuscarPatron != null : "fx:id=\"cmdBuscarPatron\" was not injected: check your FXML file 'imprimirDiccionario.fxml'.";
        assert cmdImprimirPDF != null : "fx:id=\"cmdImprimirPDF\" was not injected: check your FXML file 'imprimirDiccionario.fxml'.";
        assert lblLogo != null : "fx:id=\"lblLogo\" was not injected: check your FXML file 'imprimirDiccionario.fxml'.";
        assert txtDiccionario != null : "fx:id=\"txtDiccionario\" was not injected: check your FXML file 'imprimirDiccionario.fxml'.";
        assert cmdImprimir != null : "fx:id=\"cmdImprimir\" was not injected: check your FXML file 'imprimirDiccionario.fxml'.";
        assert cmdBorrar != null : "fx:id=\"cmdBorrar\" was not injected: check your FXML file 'imprimirDiccionario.fxml'.";

    }
}
