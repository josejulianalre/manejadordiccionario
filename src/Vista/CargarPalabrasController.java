package Vista;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * @author María Alexandra Velásquez Jaimes
 * @author José Julián Álvarez Reyes
 */

public class CargarPalabrasController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lblURLUsada;

    @FXML
    private TextField txtURLPersonal;

    @FXML
    private Button cmdCargarPalabra;

    @FXML
    private Button cmdBuscarPalabras;

    @FXML
    private Button cmdImprimirDiccionario;

    @FXML
    private Button cmdBuscarPatron;

    @FXML
    private Button cmdImprimirPDF;

    @FXML
    private Button cmdCargarURL;
    

    @FXML
    void buscarPalabras(ActionEvent event) throws IOException{
        actualizarVentana("buscarPalabra.fxml");
    }
    
    @FXML
    void buscarPatron(ActionEvent event) throws IOException {
        actualizarVentana("buscarPatron.fxml");
    }

    @FXML
    void cargarURL(ActionEvent event) {
        String aviso = "", url = "";
        Alert alert = null;
        try{
            url = "https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt";
            if(txtURLPersonal.getText() != "" || !txtURLPersonal.getText().isBlank())
                url = txtURLPersonal.getText();
            VistaPrincipalController.getDiccionario().cargar(url);
            aviso = "Palabras cargadas con exito :)";
            alert = new Alert(Alert.AlertType.INFORMATION, aviso, ButtonType.OK);
            alert.showAndWait();
        }
        catch(Exception e) {
            aviso = "Error al cargar URL: " + e.getMessage();
            alert = new Alert(Alert.AlertType.ERROR, aviso, ButtonType.OK);
            alert.showAndWait();
        }
    }

    @FXML
    void imprimirDiccionario(ActionEvent event) throws IOException {
        actualizarVentana("imprimirDiccionario.fxml");
    }

    @FXML
    void imprimirPDF(ActionEvent event) throws IOException {
        actualizarVentana("imprimirPDF.fxml");
    }
    
    private void actualizarVentana(String archivo) throws IOException {
        Stage stage = VistaPrincipalFX.app.getStage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource(archivo));
        Pane root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    
    @FXML
    void initialize() {
        assert lblURLUsada != null : "fx:id=\"lblURLUsada\" was not injected: check your FXML file 'cargarPalabras.fxml'.";
        assert txtURLPersonal != null : "fx:id=\"txtURLPersonal\" was not injected: check your FXML file 'cargarPalabras.fxml'.";
        assert cmdCargarPalabra != null : "fx:id=\"cmdCargarPalabra\" was not injected: check your FXML file 'cargarPalabras.fxml'.";
        assert cmdBuscarPalabras != null : "fx:id=\"cmdBuscarPalabras\" was not injected: check your FXML file 'cargarPalabras.fxml'.";
        assert cmdImprimirDiccionario != null : "fx:id=\"cmdImprimirDiccionario\" was not injected: check your FXML file 'cargarPalabras.fxml'.";
        assert cmdBuscarPatron != null : "fx:id=\"cmdBuscarPatron\" was not injected: check your FXML file 'cargarPalabras.fxml'.";
        assert cmdImprimirPDF != null : "fx:id=\"cmdImprimirPDF\" was not injected: check your FXML file 'cargarPalabras.fxml'.";
        assert cmdCargarURL != null : "fx:id=\"cmdCargarURL\" was not injected: check your FXML file 'cargarPalabras.fxml'.";
        
    }
}
