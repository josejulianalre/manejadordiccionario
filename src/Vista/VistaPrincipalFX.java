/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

/**
 *
 * @author José Álvarez && María Velásquez
 */
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;

public class VistaPrincipalFX extends Application{
    
    private Stage stage;
    private Scene anterior;
    public static VistaPrincipalFX app;
    
    public VistaPrincipalFX(){
      this.app = this;
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage stage) throws IOException{
        Pane root = FXMLLoader.load(getClass().getResource("vistaPrincipal.fxml"));
        
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("DICCIONARIO 3000");
        stage.resizableProperty().setValue(Boolean.FALSE);
        
        this.anterior = scene;
        this.stage = stage;
        
        stage.show();
    }
    
    public Stage getStage(){
        return this.stage;
    }//end method getStage

    public void setStage(Stage stage){
        this.stage = stage;
    }//end method setStage

    public Scene getAnterior(){
        return this.anterior;
    }//end method getAnterior

    public void setAnterior(Scene anterior){
        this.anterior = anterior;
    }//end method setAnterior

}
