package Vista;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * @author María Alexandra Velásquez Jaimes
 * @author José Julián Álvarez Reyes
 */

public class ImprimirPDFController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lblTexto;

    @FXML
    private TextField txtNumeroAIngresar;

    @FXML
    private Label lblPdfCreado;

    @FXML
    private Label lblTitulo;

    @FXML
    private Button cmdCargarPalabras;

    @FXML
    private Button cmdBuscarPalabras;

    @FXML
    private Button cmdImprimirDiccionario;

    @FXML
    private Button cmdBuscarPatron;

    @FXML
    private Button cmdImprimirPDF;

    @FXML
    private ImageView imgLogo;

    @FXML
    private Button cmdCrearPDF;

    @FXML
    void buscarPalabras(ActionEvent event) throws IOException {
        actualizarVentana("buscarPalabra.fxml");
    }

    @FXML
    void buscarPatron(ActionEvent event) throws IOException {
        actualizarVentana("buscarPatron.fxml");
    }

    @FXML
    void cargarPalabras(ActionEvent event) throws IOException {
        actualizarVentana("cargarPalabras.fxml");
    }

    @FXML
    void crearPDF(ActionEvent event) {
        int numeroPalabras = 0, tamaño;
        String aviso = "";
        Alert alert = null;
        try{
            if(VistaPrincipalController.getDiccionario().getPalabras() == null) 
                throw new Exception("Por favor primero cargue un diccionario.");
            tamaño = VistaPrincipalController.getDiccionario().getPalabras().length();
            if(txtNumeroAIngresar.getText().isBlank())
                throw new Exception("Por favor ingrese la cantidad de palabras del PDF.");
            numeroPalabras = Integer.parseInt(txtNumeroAIngresar.getText());
            if(numeroPalabras < 1 || numeroPalabras > tamaño)
                throw new Exception("Por favor asegurese que el número de palabras que desea imprimir sea mayor a 0 y menor a " + tamaño + ".");
            FileChooser fileChooser = new FileChooser();
            Stage stage = (Stage) cmdCrearPDF.getScene().getWindow();
            File selectedFile = fileChooser.showSaveDialog(stage);
            VistaPrincipalController.getDiccionario().crearPdf(numeroPalabras, selectedFile.getAbsolutePath());
            lblPdfCreado.setText("PDF creado con " + numeroPalabras + " palabras. ");
        }
        catch(Exception e) {
            aviso = "Error al crear PDF: " + e.getMessage();
            alert = new Alert(Alert.AlertType.ERROR, aviso, ButtonType.OK);
            alert.showAndWait();
        }
    }

    @FXML
    void imprimirDiccionario(ActionEvent event) throws IOException {
        actualizarVentana("imprimirDiccionario.fxml");
    }

    @FXML
    void imprimirPDF(ActionEvent event) throws IOException {
        actualizarVentana("imprimirPDF.fxml");
    }
    
    private void actualizarVentana(String archivo) throws IOException {
        Stage stage = VistaPrincipalFX.app.getStage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource(archivo));
        Pane root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    @FXML
    void initialize() {
        assert lblTexto != null : "fx:id=\"lblTexto\" was not injected: check your FXML file 'imprimirPDF.fxml'.";
        assert txtNumeroAIngresar != null : "fx:id=\"txtNumeroAIngresar\" was not injected: check your FXML file 'imprimirPDF.fxml'.";
        assert lblPdfCreado != null : "fx:id=\"lblPdfCreado\" was not injected: check your FXML file 'imprimirPDF.fxml'.";
        assert lblTitulo != null : "fx:id=\"lblTitulo\" was not injected: check your FXML file 'imprimirPDF.fxml'.";
        assert cmdCargarPalabras != null : "fx:id=\"cmdCargarPalabras\" was not injected: check your FXML file 'imprimirPDF.fxml'.";
        assert cmdBuscarPalabras != null : "fx:id=\"cmdBuscarPalabras\" was not injected: check your FXML file 'imprimirPDF.fxml'.";
        assert cmdImprimirDiccionario != null : "fx:id=\"cmdImprimirDiccionario\" was not injected: check your FXML file 'imprimirPDF.fxml'.";
        assert cmdBuscarPatron != null : "fx:id=\"cmdBuscarPatron\" was not injected: check your FXML file 'imprimirPDF.fxml'.";
        assert cmdImprimirPDF != null : "fx:id=\"cmdImprimirPDF\" was not injected: check your FXML file 'imprimirPDF.fxml'.";
        assert imgLogo != null : "fx:id=\"imgLogo\" was not injected: check your FXML file 'imprimirPDF.fxml'.";
        assert cmdCrearPDF != null : "fx:id=\"cmdCrearPDF\" was not injected: check your FXML file 'imprimirPDF.fxml'.";

    }
}