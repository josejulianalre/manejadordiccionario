/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.Diccionario;

import util.ufps.colecciones_seed.VectorGenerico;

/**
 * @author maria alexandra
 * @author jose julian
 */
public class PruebaLeerURL_Palabra {

    public void probar(String[] args) throws Exception {
        Diccionario dictionary = new Diccionario();
        //dictionary.cargar("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt");
        try {
            System.out.println(dictionary.buscarPatron("a", "xx"));
            
            dictionary.crearPdf(dictionary.getPalabras().length(), "dictionary");
        } catch (Exception e) {
            System.err.print(e);
        }
    }
}
