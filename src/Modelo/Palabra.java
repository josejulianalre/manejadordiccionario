/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Objects;
import util.ufps.colecciones_seed.VectorGenerico;

/**
 * @author María Alexandra Velásquez Jaimes
 * @author José Julián Álvarez Reyes
 */
public class Palabra implements Comparable {

    private VectorGenerico<Character> caracteres;

    public Palabra() {
    }

    /**
     * A partir de la cadena crea el vector genérico Ejemplo: Cadena="ufps",
     * this.caracteres={"u","f","p","s"}
     *
     * @param cadena un String a ser pasado al vector
     */
    public Palabra(String cadena) {
        cadena = cadena.toUpperCase();
        caracteres = new VectorGenerico(cadena.length());
        for (int i = 0; i < cadena.length(); i++) {
            caracteres.add(cadena.charAt(i));
        }
    }
    /**
     * Retorna un vector con los caracteres de la palabra
     *
     * @return Retorna un vector con los caracteres de la palabra
     */
    public VectorGenerico<Character> getCaracteres() {
        return caracteres;
    }
    /**
     * Actualiza un vector con los caracteres de la palabra
     *
     * @param caracteres un vector con los caracteres de la palabra
     */
    public void setCaracteres(VectorGenerico<Character> caracteres) {
        this.caracteres = caracteres;
    }

    
     /**
     * A partir de un patron, reemplaza en la palabra en la posicion indicada
     * 
     * @param posicion posición del inicio del patrón
     * @param patron patrón que vamos a reemplazar
     * @param patronNuevo patrón por el cual vamos a reemplazar
     */
    public void actualizar(int posicion, Palabra patron, Palabra patronNuevo) {
        int tamaño = this.caracteres.length() - (patron.getCaracteres().length() - patronNuevo.getCaracteres().length());
        VectorGenerico<Character> tmp = new VectorGenerico(tamaño);
        for (int i = 0; i < this.caracteres.length(); i++) {
            if (i == posicion) {
                for (int j = 0; j < patronNuevo.getCaracteres().length(); j++) {
                    tmp.add(patronNuevo.getCaracteres().get(j));
                }
                i += patron.getCaracteres().length() - 1;
            } else {
                tmp.add(this.caracteres.get(i));
            }
        }
        this.setCaracteres(tmp);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.caracteres);
        return hash;
    }
    /**
     * Compara si dos objetos son iguales
     * @param obj = Objeto a comparar
     * @return false si son diferentes, true si son iguales
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Palabra other = (Palabra) obj;

        if (this.caracteres.length() != other.getCaracteres().length()) {
            return false;
        }
        for (int i = 0; i < this.caracteres.length(); i++) {
            if (this.caracteres.get(i) != other.getCaracteres().get(i)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Compara dos palabras
     *
     * @param o objeto palabra a comparar
     * @return regresa -1 cuando el objeto parametro es mayor, 1 cuando el
     * objeto parametro es menor, 0 cuando el objeto parametro es igual.
     */
    @Override
    public int compareTo(Object o) {
        int compare = 0, tamaño = 0;
        if (!this.equals(o)) {
            final Palabra other = (Palabra) o;
            tamaño = this.caracteres.length();
            if (this.caracteres.length() > other.getCaracteres().length()) {
                tamaño = other.getCaracteres().length();
            }
            for (int i = 0; i < tamaño && compare == 0; i++) {
                if (this.caracteres.get(i) < other.getCaracteres().get(i)) {
                    compare = -1;
                } else if (this.caracteres.get(i) > other.getCaracteres().get(i)) {
                    compare = 1;
                }
            }
            if (compare == 0) {
                if (this.caracteres.length() > other.getCaracteres().length()) {
                    compare = 1;
                } else {
                    compare = -1;
                }
            }
        }
        return compare;
    }
}
